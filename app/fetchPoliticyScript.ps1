$MAX_LOG_SIZE = 50
$CONFIG_PATH = "./_config.json";

# Add Library
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Xml
Add-Type -AssemblyName PresentationCore,PresentationFramework

function downloadPoliticies($savedFolder, $linkToDownload){

    New-Item -ItemType Directory -Force -Path $savedFolder


    $countriesFile = "./countries.json"

    $reponse = Invoke-WebRequest -Uri $linktoDownload -OutFile $countriesFile
    
    $countries = Get-Content $countriesFile | ConvertFrom-Json
    
    printLog "Downloaded countries.json"

    $countryTotal = ($countries).Length
    $countryNum = 0
    foreach ($country in $countries) {
        $countryNum = $countryNum + 1
        printLog "[" $countryNum "/" $countryTotal  "] country: " $country.name " - " $country.code

        foreach ($legis in $country.legislatures) {
            foreach($period in $legis.legislative_periods){
                $csvFileName = $period.csv.Substring($period.csv.LastIndexOf("/") + 1)
                printLog "csv_file_name: " $csvFileName
                printLog "csv: " $period.csv_url

                $countryDir = $savedFolder + "/" + $country.code[0] + "/" + $country.slug
                New-Item -ItemType Directory -Force -Path $countryDir
                $itemFile = $countryDir + "/" + $csvFileName

                $reponse = Invoke-WebRequest -Uri $period.csv_url -OutFile $itemFile
            }
        }

        printLog "..."
        updateProcessBar $countryNum $countryTotal
    }

    printLog "==> DOWNLOADED POLITICIES SUCCESSFULLY!"
    [System.Windows.Forms.MessageBox]::Show("DOWNLOADED POLITICIES SUCCESSFULLY!", "Information", "OK", "Information")

}

function initConfig(){
    $config = Get-Content $CONFIG_PATH | ConvertFrom-Json

    $var_inpDownloadLink.Text = $config.linkToDownload
    $var_inpSaveFolder.Text = $config.savedFolder
}

function saveConfig(){
    $configObject = [PSCustomObject]@{
        linkToDownload  = $var_inpDownloadLink.Text
        savedFolder     = $var_inpSaveFolder.Text
    }

    $configObject | ConvertTo-Json -depth 100 | Out-File $CONFIG_PATH
}


function printLog() {
    [System.Windows.Forms.Application]::DoEvents()

    $currentDate = Get-Date -Format "[yyyy-MM-dd HH:mm:ss]"
    $logMessage = $currentDate + " - " + $args

    
    while ($var_viewLog.Items.Count -gt $MAX_LOG_SIZE) {
        $var_viewLog.Items.RemoveAt(0)
    }
    

    Write-Host $logMessage
    $var_viewLog.Items.Add($logMessage)
    $var_viewLog.ScrollIntoView($var_viewLog.Items.GetItemAt($var_viewLog.Items.Count - 1))
}

function updateProcessBar($currentVal, $maxValue){
    $var_processBar.Value = ($currentVal * 100) / $maxValue
}

function addHandlerButtons() {

    $var_btnDownload.Add_Click({
        updateEnableButton($false)
        updateProcessBar 0 1
        
        printLog "download politicy from link: " $var_inpDownloadLink.Text
        printLog "save to folder: " $var_inpSaveFolder.Text
        saveConfig

        downloadPoliticies $var_inpSaveFolder.Text $var_inpDownloadLink.Text

        updateEnableButton($true)
        updateProcessBar 1 1
    })
}

function updateEnableButton($enabled) {
    $var_btnDownload.IsEnabled = $enabled;
}

function displayApp() {
    # where is the XAML file?
    $xamlFile = ".\FetchPoliticyGUI\FetchPoliticyGUI\MainWindow.xaml"

    #create window
    $inputXML = Get-Content $xamlFile -Raw
    $inputXML = $inputXML -replace 'mc:Ignorable="d"', '' -replace "x:N", 'N' -replace '^<Win.*', '<Window'
    [XML]$XAML = $inputXML

    #Read XAML
    $reader = (New-Object System.Xml.XmlNodeReader $xaml)
    try {
        # [System.Reflection.Assembly]::LoadWithPartialName(‘PresentationFramework’) | Out-Null
        $window = [Windows.Markup.XamlReader]::Load( $reader )
    } catch {
        Write-Warning $_.Exception
        throw
    }

    # Create variables based on form control names.
    # Variable will be named as 'var_<control name>'

    $xaml.SelectNodes("//*[@Name]") | ForEach-Object {
        #"trying item $($_.Name)"
        try {
            Set-Variable -Name "var_$($_.Name)" -Value $window.FindName($_.Name) -ErrorAction Stop
        } catch {
            throw
        }
    }
    Get-Variable var_*

    initConfig
    addHandlerButtons
    printLog ">> Welcome to App >>"

    $window.Add_Closed({
        quit
    })

    $Null = $window.ShowDialog()
    quit
}

function quit(){
    Write-Host "........................STOP APP............................."
    Break Script
}

#### MAIN ###################################################
displayApp